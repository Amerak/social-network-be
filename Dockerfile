FROM openjdk:17-jdk-alpine

EXPOSE 8080
RUN mkdir /app

COPY /social-network-be-1.0.0-RELEASE.jar /app/social-network-application.jar

ENTRYPOINT ["java", "-jar", "/app/social-network-application.jar"]
