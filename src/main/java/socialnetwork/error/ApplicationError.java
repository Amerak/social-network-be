package socialnetwork.error;

public class ApplicationError extends RuntimeException {

    private ErrorCode code;

    public ApplicationError(String message, ErrorCode code, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public ErrorCode getCode() {
        return code;
    }

    public void setCode(ErrorCode code) {
        this.code = code;
    }
}
