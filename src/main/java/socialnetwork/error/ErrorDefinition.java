package socialnetwork.error;

public interface ErrorDefinition {
    String getMessage();

    ErrorCode getErrorCode();
}
