package socialnetwork.error;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;

@ControllerAdvice
public class ErrorController {

    @Inject
    private ErrorProcessor errorProcessor;

    @ExceptionHandler(ApplicationError.class)
    public ResponseEntity<Object> handleException(final RuntimeException exception, final WebRequest webRequest) {
        return errorProcessor.processException(exception, webRequest);
    }

}
