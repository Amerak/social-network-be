package socialnetwork.error;

public class DefaultErrorCode implements ErrorCode {

    private final String code;

    private final ErrorType type;

    public DefaultErrorCode(String code, ErrorType type) {
        this.code = code;
        this.type = type;
    }

    @Override
    public String getErrorCode() {
        return code;
    }

    @Override
    public ErrorType getType() {
        return type;
    }
}
