package socialnetwork.error;

import java.io.Serializable;

public interface ErrorCode extends Serializable {

    String getErrorCode();

    ErrorType getType();

    static ErrorCode system(String code) {
        return new DefaultErrorCode(code, ErrorType.SYSTEM);
    }

    static ErrorCode application(String code) {
        return new DefaultErrorCode(code, ErrorType.APPLICATION);
    }
}
