package socialnetwork.error;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import socialnetwork.helper.HttpStatusHelper;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import java.util.Map;

@Component
public class ErrorProcessor extends ResponseEntityExceptionHandler {

    @Inject
    private ErrorAttributes defaultErrorAttributes;

    public ResponseEntity<Object> processException(final RuntimeException exception, final WebRequest webRequest) {

        HttpStatus status = HttpStatusHelper.resolveStatus((ApplicationError) exception);
        webRequest.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, status.value(), RequestAttributes.SCOPE_REQUEST);

        ErrorAttributeOptions errorAttributeOptions = ErrorAttributeOptions.of(ErrorAttributeOptions.Include.STACK_TRACE);
        final Map<String, Object> errorAttributes = defaultErrorAttributes.getErrorAttributes(webRequest, errorAttributeOptions);

        return handleExceptionInternal(exception, errorAttributes, new HttpHeaders(), HttpStatus.BAD_REQUEST, webRequest);
    }
}
