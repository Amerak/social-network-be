package socialnetwork.configuration;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
import socialnetwork.error.ApplicationError;
import socialnetwork.helper.HttpStatusHelper;

import javax.servlet.RequestDispatcher;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public LocaleResolver localeResolver() {
        Locale.setDefault(Locale.ENGLISH);
        return new FixedLocaleResolver(Locale.ENGLISH);
    }

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, options);

                Throwable error = getError(webRequest);

                if (error instanceof ApplicationError exception) {
                    errorAttributes.put("errorCode", exception.getCode().getErrorCode());
                    errorAttributes.put("message", exception.getMessage());
                    errorAttributes.put("status", HttpStatusHelper.resolveStatus(exception).value());

                    if (exception.getCause() instanceof ConstraintViolationException constraintViolationException) {
                        List<Violation> violationsDtoOut = new ArrayList<>();
                        List<ConstraintViolation<?>> violations = new ArrayList<>(constraintViolationException.getConstraintViolations());
                        violations.forEach(violation -> {
                            Violation violationOut = new Violation();
                            violationOut.setCode(violation.getPropertyPath().toString());
                            violationOut.setMessage(violation.getMessage());
                            violationsDtoOut.add(violationOut);
                        });
                        errorAttributes.put("cause", violationsDtoOut);
                    }
                }

                webRequest.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, HttpStatus.BAD_REQUEST.value(), RequestAttributes.SCOPE_REQUEST);
                return errorAttributes;
            }
        };
    }
}
