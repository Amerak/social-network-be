package socialnetwork.websocket;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import socialnetwork.domain.Message;
import socialnetwork.helper.MessageHelper;
import socialnetwork.model.websocket.SaveMessageWebsocketModel;
import socialnetwork.model.websocket.SetUserActiveWebsocketModel;
import socialnetwork.model.websocket.SetUserNotActiveWebsocketModel;
import socialnetwork.utils.SessionUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class ChatWebSocketHandler extends TextWebSocketHandler {

    @Inject
    private SaveMessageWebsocketModel saveMessageWebsocketModel;
    @Inject
    private SetUserActiveWebsocketModel setUserActiveWebsocketModel;
    @Inject
    private SetUserNotActiveWebsocketModel setUserNotActiveWebsocketModel;
    @Inject
    private MessageHelper messageHelper;
    private final List<WebSocketSession> webSocketSessions = new ArrayList<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        setUserActiveWebsocketModel.setUserActive(session);
        webSocketSessions.add(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        Message savedMessage = saveMessageWebsocketModel.saveMessage(session, messageHelper.decode(message));
        for(WebSocketSession webSocketSession : webSocketSessions){
            if(SessionUtils.isValidForSend(session, webSocketSession) || SessionUtils.isValidForNotify(session, webSocketSession)){
                webSocketSession.sendMessage(messageHelper.encode(savedMessage));
            }
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        webSocketSessions.remove(session);
        Object[] sessions = webSocketSessions.stream().filter(webSocketSession -> Objects.requireNonNull(webSocketSession.getPrincipal()).getName().equals(Objects.requireNonNull(session.getPrincipal()).getName())).toArray();
        if(sessions.length == 0){
            setUserNotActiveWebsocketModel.setUserNotActive(session);
        }
    }
}
