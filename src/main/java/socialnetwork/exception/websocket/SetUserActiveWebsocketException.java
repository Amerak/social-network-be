package socialnetwork.exception.websocket;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class SetUserActiveWebsocketException extends ApplicationError {
    private static final String ERROR_PREFIX = "websocket/setUserActive/";

    public SetUserActiveWebsocketException(SetUserActiveWebsocketException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {

        USER_DAO_SAVE_FAILED("userDaoSaveFailed", "Updating user using user DAO save failed.", ErrorType.SYSTEM),
        USER_NOT_FOUND("userNotFound","User not found",ErrorType.APPLICATION);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
