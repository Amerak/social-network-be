package socialnetwork.exception.comment;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class CreateCommentException extends ApplicationError {
    private static final String ERROR_PREFIX = "comment/create/";

    public CreateCommentException(CreateCommentException.Error error) {
        super(error.getMessage(), error.getErrorCode(), null);
    }

    public CreateCommentException(CreateCommentException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {

        COMMENT_DAO_CREATE_FAILED("commentDaoCreateFailed", "Creating comment using comment DAO save failed.", ErrorType.SYSTEM),
        INVALID_DTO_IN("invalidDtoIn", "DtoIn is not valid.", ErrorType.APPLICATION),
        POST_NOT_FOUND("postNotFound", "Post not found!", ErrorType.APPLICATION);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
