package socialnetwork.exception.comment;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class DeleteCommentException extends ApplicationError {
    private static final String ERROR_PREFIX = "comment/delete/";

    public DeleteCommentException(DeleteCommentException.Error error) {
        super(error.getMessage(), error.getErrorCode(), null);
    }

    public DeleteCommentException(DeleteCommentException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {

        COMMENT_DAO_DELETE_FAILED("commentDaoDeleteFailed", "Deleting comment using comment DAO save failed.", ErrorType.SYSTEM),
        COMMENT_DAO_FIND_FAILED("commentDaoFindFailed", "Receiving comment using comment DAO find failed.", ErrorType.SYSTEM),
        INVALID_DTO_IN("invalidDtoIn", "DtoIn is not valid.", ErrorType.APPLICATION),
        USER_NOT_AUTHORIZED("userNotAuthorized", "User is not authorized to delete this comment.", ErrorType.APPLICATION),
        COMMENT_NOT_FOUND("commentNotFound", "Comment with selected id was not found.", ErrorType.APPLICATION);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
