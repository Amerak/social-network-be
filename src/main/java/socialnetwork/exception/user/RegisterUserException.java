package socialnetwork.exception.user;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class RegisterUserException extends ApplicationError {
    private static final String ERROR_PREFIX = "user/register/";

    public RegisterUserException(RegisterUserException.Error error) {
        super(error.getMessage(), error.getErrorCode(), null);
    }

    public RegisterUserException(RegisterUserException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {
        INVALID_DTO_IN("invalidDtoIn", "DtoIn is not valid.", ErrorType.APPLICATION),
        USER_DAO_INSERT_FAILED("userDaoInsertFailed", "Creating User by user DAO insert failed.", ErrorType.SYSTEM),
        USERNAME_ALREADY_EXISTS("usernameAlreadyExists", "User with this username already exists.", ErrorType.APPLICATION),
        EMAIL_ALREADY_EXISTS("emailAlreadyExists", "User with this email already exists.", ErrorType.APPLICATION);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
