package socialnetwork.exception.message;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class FindListMessageException extends ApplicationError {
    private static final String ERROR_PREFIX = "message/list/";

    public FindListMessageException(FindListMessageException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {
        INVALID_DTO_IN("invalidDtoIn", "DtoIn is not valid", ErrorType.APPLICATION),
        MESSAGE_DAO_FIND_FAILED("messageDaoFindFailed","Receiving messages using message DAO find failed.", ErrorType.SYSTEM);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
