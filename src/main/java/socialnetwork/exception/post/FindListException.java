package socialnetwork.exception.post;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class FindListException extends ApplicationError {
    private static final String ERROR_PREFIX = "post/list/";

    public FindListException(FindListException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {
        POST_DAO_LIST_FAILED("postDaoListFailed", "Receiving posts using post DAO list failed.", ErrorType.SYSTEM);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
