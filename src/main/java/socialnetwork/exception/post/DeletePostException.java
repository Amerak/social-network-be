package socialnetwork.exception.post;

import socialnetwork.error.ApplicationError;
import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorDefinition;
import socialnetwork.error.ErrorType;
import socialnetwork.helper.ExceptionHelper;

public class DeletePostException extends ApplicationError {
    private static final String ERROR_PREFIX = "post/delete/";

    public DeletePostException(DeletePostException.Error error) {
        super(error.getMessage(), error.getErrorCode(), null);
    }

    public DeletePostException(DeletePostException.Error error, Throwable cause) {
        super(error.getMessage(), error.getErrorCode(), cause);
    }

    public enum Error implements ErrorDefinition {

        POST_DAO_DELETE_FAILED("postDaoDeleteFailed", "Deleting post by post DAO delete failed", ErrorType.SYSTEM),
        INVALID_DTO_IN("invalidDtoIn", "DtoIn is not valid.", ErrorType.APPLICATION),
        USER_NOT_AUTHORIZED("userNotAuthorized", "User is not authorized to delete this post.", ErrorType.APPLICATION),
        POST_NOT_FOUND("postNotFound", "Post with selected id was not found.", ErrorType.APPLICATION),
        POST_DAO_FIND_FAILED("postDaoFindFailed", "Receiving post using post DAO find failed.", ErrorType.SYSTEM);

        private String code;
        private String message;
        private ErrorType type;

        Error(String code, String message, ErrorType type) {
            this.code = ERROR_PREFIX + code;
            this.message = message;
            this.type = type;
        }

        public ErrorType getType() {
            return type;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ErrorCode getErrorCode() {
            return ExceptionHelper.createErrorCode(code, type);
        }
    }
}
