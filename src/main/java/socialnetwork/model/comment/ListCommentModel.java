package socialnetwork.model.comment;

import org.springframework.stereotype.Component;
import socialnetwork.dao.CommentDao;
import socialnetwork.domain.Comment;
import socialnetwork.dto.comment.ListCommentDtoIn;
import socialnetwork.dto.comment.ListCommentDtoOut;
import socialnetwork.exception.comment.ListCommentException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Component
public class ListCommentModel {

    @Inject
    private CommentDao commentDao;

    @Inject
    private Validator validator;

    public ListCommentDtoOut listComment(ListCommentDtoIn dtoIn) {
        Set<ConstraintViolation<ListCommentDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new ListCommentException(ListCommentException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        List<Comment> commentList = list(dtoIn.getPostId());

        return new ListCommentDtoOut(commentList);
    }

    private List<Comment> list(String postId) {
        try {
            return commentDao.findByPostIdInOrderByDateAsc(postId);
        } catch (Exception e) {
            throw new ListCommentException(ListCommentException.Error.COMMENT_DAO_LIST_FAILED, e);
        }
    }
}
