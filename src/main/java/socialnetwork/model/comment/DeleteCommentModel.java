package socialnetwork.model.comment;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.CommentDao;
import socialnetwork.domain.Comment;
import socialnetwork.dto.comment.DeleteCommentDtoIn;
import socialnetwork.dto.comment.DeleteCommentDtoOut;
import socialnetwork.exception.comment.DeleteCommentException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;

@Component
public class DeleteCommentModel {

    @Inject
    private CommentDao commentDao;
    @Inject
    private Validator validator;

    public DeleteCommentDtoOut deleteComment(DeleteCommentDtoIn dtoIn) {
        Set<ConstraintViolation<DeleteCommentDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new DeleteCommentException(DeleteCommentException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        checkAuthorization(dtoIn);

        delete(dtoIn);

        return new DeleteCommentDtoOut("Comment deleted successfully.");
    }

    public void checkAuthorization(DeleteCommentDtoIn dtoIn){
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Comment comment = getComment(dtoIn);
        if(!comment.getUserId().equals(springSecurityUser.getId())){
            throw new DeleteCommentException(DeleteCommentException.Error.USER_NOT_AUTHORIZED);
        }
    }

    private Comment getComment(DeleteCommentDtoIn dtoIn){
        Optional<Comment> comment;
        try{
            comment = commentDao.findById(dtoIn.getCommentId());
        }catch (Exception e) {
            throw new DeleteCommentException(DeleteCommentException.Error.COMMENT_DAO_FIND_FAILED, e);
        }

        if(comment.isEmpty()){
            throw new DeleteCommentException(DeleteCommentException.Error.COMMENT_NOT_FOUND);
        }

        return comment.get();
    }

    private void delete(DeleteCommentDtoIn dtoIn) {
        try {
            commentDao.deleteById(dtoIn.getCommentId());
        } catch (Exception e) {
            throw new DeleteCommentException(DeleteCommentException.Error.COMMENT_DAO_DELETE_FAILED, e);
        }
    }
}
