package socialnetwork.model.comment;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.CommentDao;
import socialnetwork.dao.PostDao;
import socialnetwork.domain.Comment;
import socialnetwork.domain.Post;
import socialnetwork.dto.comment.CreateCommentDtoIn;
import socialnetwork.dto.comment.CreateCommentDtoOut;
import socialnetwork.exception.comment.CreateCommentException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Calendar;
import java.util.Optional;
import java.util.Set;

@Component
public class CreateCommentModel {

    @Inject
    private CommentDao commentDao;
    @Inject
    private PostDao postDao;
    @Inject
    private Validator validator;

    public CreateCommentDtoOut createComment(CreateCommentDtoIn dtoIn) {
        Set<ConstraintViolation<CreateCommentDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new CreateCommentException(CreateCommentException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        create(dtoIn);

        return new CreateCommentDtoOut("Comment created successfully");
    }

    private void create(CreateCommentDtoIn dtoIn) {
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Comment comment = new Comment();
        comment.setContent(dtoIn.getContent());
        comment.setDate(Calendar.getInstance().getTime());
        comment.setUserId(springSecurityUser.getId());
        comment.setUsername(springSecurityUser.getUsername());

        Post post = findPostById(dtoIn.getPostId());
        comment.setPostId(post.getId());

        try {
            commentDao.save(comment);
        } catch (Exception e) {
            throw new CreateCommentException(CreateCommentException.Error.COMMENT_DAO_CREATE_FAILED, e);
        }
    }

    private Post findPostById(String id) {
        Optional<Post> post = postDao.findById(id);
        if (post.isEmpty()) {
            throw new CreateCommentException(CreateCommentException.Error.POST_NOT_FOUND);
        }
        return post.get();
    }
}
