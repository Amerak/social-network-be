package socialnetwork.model.dataGeneration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import socialnetwork.dao.CommentDao;
import socialnetwork.dao.MessageDao;
import socialnetwork.dao.PostDao;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.Comment;
import socialnetwork.domain.Post;
import socialnetwork.domain.User;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component
public class GenerateDataModel {

    @Inject
    private UserDao userDao;
    @Inject
    private PostDao postDao;
    @Inject
    private CommentDao commentDao;
    @Inject
    private MessageDao messageDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void generateData() {
        deleteAllData();

        List<String> initList = createUsers();

        createComments(createPosts(initList));
    }

    private void deleteAllData(){
        commentDao.deleteAll();
        messageDao.deleteAll();
        postDao.deleteAll();
        userDao.deleteAll();
    }

    private List<String> createUsers() {
        List<String> initList = new ArrayList<>();
        User user = new User();
        user.setFirstName("Michal");
        user.setSurname("Kořínek");
        user.setUsername("michal99");
        user.setPassword(passwordEncoder.encode("testPassword"));
        user.setEmail("michal@testmail.cz");
        userDao.save(user);
        initList.add(user.getId()); // index 0
        initList.add("michal99"); // index 1

        User user2 = new User();
        user2.setFirstName("Dominik");
        user2.setSurname("Lohniský");
        user2.setUsername("dominik98");
        user2.setPassword(passwordEncoder.encode("testPassword"));
        user2.setEmail("dominik@testmail.cz");
        userDao.save(user2);
        initList.add(user2.getId()); // index 2
        initList.add("dominik98"); // index 3
        return initList;
    }

    private List<String> createPosts(List<String> initList) {
        Post post = new Post();
        post.setContent("This is the test content of first post, which should have id of the first user named Michal Kořínek");
        post.setHeader("Test header of the post");
        post.setDate(Calendar.getInstance().getTime());
        post.setUserId(initList.get(0));
        post.setUsername(initList.get(1));
        postDao.save(post);
        initList.add(post.getId());// index 4

        Post post2 = new Post();
        post2.setContent("This is the test content of second post, which should have id of the second user named Dominik Lohniský.");
        post2.setHeader("Test header of the second post");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        post2.setDate(calendar.getTime());
        post2.setUserId(initList.get(2));
        post2.setUsername(initList.get(3));
        postDao.save(post2);
        initList.add(post2.getId()); // index 5

        return initList;
    }

    private void createComments(List<String> initList) {

        Comment comment = new Comment();
        comment.setContent("This post is so cool!");
        comment.setDate(Calendar.getInstance().getTime());
        comment.setUserId(initList.get(0));
        comment.setUsername(initList.get(1));
        comment.setPostId(initList.get(4));
        commentDao.save(comment);

        Comment comment2 = new Comment();
        comment2.setContent("This second post is also cool!");
        comment2.setDate(Calendar.getInstance().getTime());
        comment2.setUserId(initList.get(2));
        comment2.setUsername(initList.get(3));
        comment2.setPostId(initList.get(5));

        commentDao.save(comment2);
    }
}
