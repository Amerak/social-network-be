package socialnetwork.model.message;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.MessageDao;
import socialnetwork.domain.Message;
import socialnetwork.dto.message.ListMessageDtoIn;
import socialnetwork.dto.message.ListMessageDtoOut;
import socialnetwork.exception.message.FindListMessageException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Component
public class FindListMessageModel {
    @Inject
    private MessageDao messageDao;
    @Inject
    private Validator validator;

    public ListMessageDtoOut findList(ListMessageDtoIn dtoIn) {
        Set<ConstraintViolation<ListMessageDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new FindListMessageException(FindListMessageException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        return new ListMessageDtoOut(findMessages(dtoIn));
    }

    public List<Message> findMessages(ListMessageDtoIn dtoIn) {
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String fromUsername = springSecurityUser.getUsername();

        try {
            return messageDao.findPositionalParameters(fromUsername, dtoIn.getTo());
        } catch (Exception e) {
            throw new FindListMessageException(FindListMessageException.Error.MESSAGE_DAO_FIND_FAILED, e);
        }
    }
}
