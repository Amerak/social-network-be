package socialnetwork.model.user;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.UnfollowUserDtoIn;
import socialnetwork.dto.user.UnfollowUserDtoOut;
import socialnetwork.exception.user.UnfollowUserException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;

@Component
public class UnfollowUserModel {

    @Inject
    private UserDao userDao;

    @Inject
    private Validator validator;

    public UnfollowUserDtoOut unfollowUser(UnfollowUserDtoIn dtoIn) {
        Set<ConstraintViolation<UnfollowUserDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new UnfollowUserException(UnfollowUserException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        unfollow(dtoIn);

        return new UnfollowUserDtoOut("Unfollowed successfully");
    }

    private void unfollow(UnfollowUserDtoIn dtoIn) {
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = getUserByID(springSecurityUser.getId());
        try {
            if (user.getFollowList() != null && !user.getFollowList().isEmpty()) {
                user.getFollowList().remove(dtoIn.getUserId());
                userDao.save(user);
            }
        } catch (Exception e) {
            throw new UnfollowUserException(UnfollowUserException.Error.USER_DAO_SAVE_FAILED, e);
        }
    }

    private User getUserByID(String id) {
        Optional<User> user = userDao.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else throw new UnfollowUserException(UnfollowUserException.Error.USER_NOT_FOUND);
    }
}
