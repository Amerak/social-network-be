package socialnetwork.model.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.RegisterUserDtoIn;
import socialnetwork.dto.user.RegisterUserDtoOut;
import socialnetwork.exception.user.RegisterUserException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

@Component
public class RegisterUserModel {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Inject
    private Validator validator;

    @Inject
    private UserDao userDao;

    public RegisterUserDtoOut register(RegisterUserDtoIn dtoIn) {
        Set<ConstraintViolation<RegisterUserDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new RegisterUserException(RegisterUserException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        dtoIn.setPassword(passwordEncoder.encode(dtoIn.getPassword()));
        User user = createUser(dtoIn);

        return new RegisterUserDtoOut(user.getId(), user.getUsername());
    }

    private User createUser(RegisterUserDtoIn dtoIn) {
        try {
            User user = new User(dtoIn.getFirstName(), dtoIn.getSurname(), dtoIn.getUsername(), dtoIn.getPassword(), dtoIn.getEmail());
            return userDao.insert(user);
        } catch (DuplicateKeyException e) {
            String message = e.getCause().getMessage();
            String duplicates = message.substring(message.indexOf("{", message.indexOf("{") + 1), message.indexOf("}") + 1);

            if (duplicates.contains("email:")) {
                throw new RegisterUserException(RegisterUserException.Error.EMAIL_ALREADY_EXISTS, e);
            } else if (duplicates.contains("username:")) {
                throw new RegisterUserException(RegisterUserException.Error.USERNAME_ALREADY_EXISTS, e);
            }
            throw new RegisterUserException(RegisterUserException.Error.USER_DAO_INSERT_FAILED, e);

        } catch (Exception e) {
            throw new RegisterUserException(RegisterUserException.Error.USER_DAO_INSERT_FAILED, e);
        }
    }
}
