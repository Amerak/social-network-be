package socialnetwork.model.user;

import org.springframework.stereotype.Component;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.GetUserListDtoIn;
import socialnetwork.dto.user.GetUserListDtoOut;
import socialnetwork.exception.user.FindUserListException;

import javax.inject.Inject;
import java.util.List;

@Component
public class FindListUserModel {

    @Inject
    private UserDao userDao;

    public GetUserListDtoOut findList(GetUserListDtoIn dtoIn) {
        List<User> userList;

        try {
            if (dtoIn.getUsers() != null && !dtoIn.getUsers().isEmpty()) {
                userList = getListByIds(dtoIn);
            } else {
                userList = getList();
            }
        } catch (Exception e) {
            throw new FindUserListException(FindUserListException.Error.USER_DAO_FIND_FAILED, e);
        }

        return new GetUserListDtoOut(userList);
    }

    private List<User> getListByIds(GetUserListDtoIn dtoIn) {
        return userDao.findByIdIn(dtoIn.getUsers());
    }

    private List<User> getList() {
        return userDao.findAll();
    }
}
