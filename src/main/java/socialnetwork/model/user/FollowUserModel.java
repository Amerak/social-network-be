package socialnetwork.model.user;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.FollowUserDtoIn;
import socialnetwork.dto.user.FollowUserDtoOut;
import socialnetwork.exception.user.FollowUserException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class FollowUserModel {

    @Inject
    private UserDao userDao;

    @Inject
    private Validator validator;

    public FollowUserDtoOut followUser(FollowUserDtoIn dtoIn) {
        Set<ConstraintViolation<FollowUserDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new FollowUserException(FollowUserException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        follow(dtoIn);

        return new FollowUserDtoOut("Successfully added");

    }

    private void follow(FollowUserDtoIn dtoIn) {
        getUserByID(dtoIn.getUserId());
        try {
            SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User user = getUserByID(springSecurityUser.getId());
            if (user.getFollowList() == null) {
                List<String> list = new ArrayList<>();
                list.add(dtoIn.getUserId());
                user.setFollowList(list);
            } else {
                if (!user.getFollowList().contains(dtoIn.getUserId())) {
                    user.getFollowList().add(dtoIn.getUserId());
                }
            }
            userDao.save(user);
        } catch (RuntimeException e) {
            throw new FollowUserException(FollowUserException.Error.USER_DAO_SAVE_FAILED, e);
        }
    }

    private User getUserByID(String id) {
        Optional<User> user = userDao.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new FollowUserException(FollowUserException.Error.USER_NOT_FOUND);
        }

    }

}
