package socialnetwork.model.user;

import org.springframework.stereotype.Component;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.GetUserDtoIn;
import socialnetwork.dto.user.GetUserDtoOut;
import socialnetwork.exception.user.GetUserException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;

@Component
public class GetUserModel {

    @Inject
    private UserDao userDao;

    @Inject
    private Validator validator;

    public GetUserDtoOut findUserByID(GetUserDtoIn dtoIn) {
        Set<ConstraintViolation<GetUserDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new GetUserException(GetUserException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        User user = getUserByID(dtoIn.getId());

        return new GetUserDtoOut(
                user.getId(),
                user.getFirstName(),
                user.getSurname(),
                user.getUsername(),
                user.getEmail(),
                user.isActive());
    }

    private User getUserByID(String id) {
        Optional<User> user = userDao.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new GetUserException(GetUserException.Error.USER_NOT_FOUND);
        }

    }
}
