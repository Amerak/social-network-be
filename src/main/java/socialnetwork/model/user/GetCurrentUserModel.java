package socialnetwork.model.user;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.GetCurrentUserDtoOut;
import socialnetwork.exception.user.GetUserCurrentException;

import javax.inject.Inject;
import java.util.Optional;

@Component
public class GetCurrentUserModel {

    @Inject
    private UserDao userDao;

    public GetCurrentUserDtoOut getCurrentUser() {
        User user = getUser();

        return new GetCurrentUserDtoOut(
                user.getId(),
                user.getFirstName(),
                user.getSurname(),
                user.getUsername(),
                user.getEmail(),
                user.getFollowList()
        );
    }

    private User getUser() {
        Optional<User> user;
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            user = userDao.findById(springSecurityUser.getId());
        } catch (Exception e) {
            throw new GetUserCurrentException(GetUserCurrentException.Error.USER_DAO_FIND_FAILED, e);
        }

        if (user.isEmpty()) {
            throw new GetUserCurrentException(GetUserCurrentException.Error.USER_NOT_FOUND);
        }

        return user.get();
    }
}
