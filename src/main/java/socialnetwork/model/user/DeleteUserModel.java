package socialnetwork.model.user;

import org.springframework.stereotype.Component;
import socialnetwork.dao.UserDao;
import socialnetwork.dto.user.DeleteUserDtoIn;
import socialnetwork.dto.user.DeleteUserDtoOut;
import socialnetwork.exception.user.DeleteUserException;

import javax.inject.Inject;

@Component
public class DeleteUserModel {

    @Inject
    private UserDao userDao;

    public DeleteUserDtoOut deleteUser(DeleteUserDtoIn dtoIn) {
        deleteUserByID(dtoIn.getId());

        return new DeleteUserDtoOut("Successfuly deleted!");
    }

    private void deleteUserByID(String id) {
        try {
            userDao.deleteById(id);
        } catch (Exception e) {
            throw new DeleteUserException(DeleteUserException.Error.USER_DAO_DELETE_FAILED, e);
        }
    }
}
