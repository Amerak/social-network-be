package socialnetwork.model.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.EditUserDtoIn;
import socialnetwork.dto.user.EditUserDtoOut;
import socialnetwork.exception.user.EditUserException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;

@Component
public class EditUserModel {

    @Inject
    private UserDao userDao;

    @Inject
    private Validator validator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public EditUserDtoOut editUser(EditUserDtoIn dtoIn) {
        Set<ConstraintViolation<EditUserDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new EditUserException(EditUserException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        editUserById(dtoIn);

        return new EditUserDtoOut("Successfuly changed!");
    }

    private void editUserById(EditUserDtoIn dtoIn) {
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = getUserByID(springSecurityUser.getId());
        try {
            if (!ObjectUtils.isEmpty(dtoIn.getFirstName())) {
                user.setFirstName(dtoIn.getFirstName());
            }
            if (!ObjectUtils.isEmpty(dtoIn.getSurname())) {
                user.setSurname(dtoIn.getSurname());
            }
            if (!ObjectUtils.isEmpty(dtoIn.getPassword())) {
                user.setPassword(passwordEncoder.encode(dtoIn.getPassword()));
            }
            userDao.save(user);
        } catch (Exception ex) {
            throw new EditUserException(EditUserException.Error.USER_DAO_SAVE_FAILED);
        }
    }

    private User getUserByID(String id) {
        Optional<User> user = userDao.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new EditUserException(EditUserException.Error.USER_NOT_FOUND);
        }
    }
}
