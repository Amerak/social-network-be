package socialnetwork.model.post;

import org.springframework.stereotype.Component;
import socialnetwork.dao.PostDao;
import socialnetwork.domain.Post;
import socialnetwork.dto.post.GetPostListDtoIn;
import socialnetwork.dto.post.GetPostListDtoOut;
import socialnetwork.exception.post.FindListException;

import javax.inject.Inject;
import java.util.List;

@Component
public class FindListPostModel {

    @Inject
    private PostDao postDao;

    public GetPostListDtoOut findList(GetPostListDtoIn dtoIn) {
        List<Post> postList;
        try {
            if (dtoIn.getUsers() != null && !dtoIn.getUsers().isEmpty()) {
                postList = getListByIds(dtoIn);
            } else {
                postList = getList();
            }
        } catch (Exception e) {
            throw new FindListException(FindListException.Error.POST_DAO_LIST_FAILED, e);
        }

        return new GetPostListDtoOut(postList);
    }

    private List<Post> getListByIds(GetPostListDtoIn dtoIn) {
        return postDao.findByUserIdInOrderByDateDesc(dtoIn.getUsers());
    }

    private List<Post> getList() {
        return postDao.findAllByOrderByDateDesc();
    }

}
