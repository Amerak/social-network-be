package socialnetwork.model.post;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.PostDao;
import socialnetwork.domain.Post;
import socialnetwork.dto.post.CreatePostDtoIn;
import socialnetwork.dto.post.CreatePostDtoOut;
import socialnetwork.exception.post.CreatePostException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Calendar;
import java.util.Set;

@Component
public class CreatePostModel {

    @Inject
    private Validator validator;

    @Inject
    private PostDao postDao;

    public CreatePostDtoOut createPost(CreatePostDtoIn dtoIn) {
        Set<ConstraintViolation<CreatePostDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new CreatePostException(CreatePostException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        Post post = create(dtoIn);

        return new CreatePostDtoOut(post.getId(), "Post created successfully under this ID");
    }

    private Post create(CreatePostDtoIn dtoIn) {
        try {
            Post post = new Post();
            post.setContent(dtoIn.getContent());
            post.setHeader(dtoIn.getHeader());
            post.setDate(Calendar.getInstance().getTime());
            SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            post.setUserId(springSecurityUser.getId());
            post.setUsername(springSecurityUser.getUsername());
            return postDao.insert(post);
        } catch (Exception e) {
            throw new CreatePostException(CreatePostException.Error.POST_DAO_CREATE_FAILED, e);
        }

    }
}
