package socialnetwork.model.post;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.PostDao;
import socialnetwork.domain.Post;
import socialnetwork.dto.post.DeletePostDtoIn;
import socialnetwork.dto.post.DeletePostDtoOut;
import socialnetwork.exception.post.DeletePostException;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;

@Component
public class DeletePostModel {

    @Inject
    private Validator validator;

    @Inject
    private PostDao postDao;

    public DeletePostDtoOut deletePost(DeletePostDtoIn dtoIn) {
        Set<ConstraintViolation<DeletePostDtoIn>> violations = validator.validate(dtoIn);
        if (!violations.isEmpty()) {
            throw new DeletePostException(DeletePostException.Error.INVALID_DTO_IN, new ConstraintViolationException(violations));
        }

        checkAuthorization(dtoIn);

        delete(dtoIn);

        return new DeletePostDtoOut("Post deleted successfully");
    }

    public void checkAuthorization(DeletePostDtoIn dtoIn){
        SpringSecurityUser springSecurityUser = (SpringSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Post post = getPost(dtoIn);
        if(!post.getUserId().equals(springSecurityUser.getId())){
            throw new DeletePostException(DeletePostException.Error.USER_NOT_AUTHORIZED);
        }
    }


    private Post getPost(DeletePostDtoIn dtoIn){
        Optional<Post> post;
        try{
            post = postDao.findById(dtoIn.getPostId());
        }catch (Exception e) {
            throw new DeletePostException(DeletePostException.Error.POST_DAO_FIND_FAILED, e);
        }

        if(post.isEmpty()){
            throw new DeletePostException(DeletePostException.Error.POST_NOT_FOUND);
        }

        return post.get();
    }

    private void delete(DeletePostDtoIn dtoIn) {
        try{
            postDao.deleteById(dtoIn.getPostId());
        }catch (Exception e){
            throw new DeletePostException(DeletePostException.Error.POST_DAO_DELETE_FAILED, e);
        }
    }
}
