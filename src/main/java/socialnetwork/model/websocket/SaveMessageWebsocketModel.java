package socialnetwork.model.websocket;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;
import socialnetwork.dao.MessageDao;
import socialnetwork.domain.Message;
import socialnetwork.exception.websocket.SaveMessageWebsocketException;
import socialnetwork.utils.SessionUtils;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Objects;

@Component
public class SaveMessageWebsocketModel {

    @Inject
    private MessageDao messageDao;

    public Message saveMessage(WebSocketSession session, Message message) {
        try{
            message.setFrom(Objects.requireNonNull(session.getPrincipal()).getName());
            message.setDate(Calendar.getInstance().getTime());
            message.setTo(SessionUtils.getLastPathSegment(session));
            messageDao.save(message);
            return message;
        } catch (Exception e){
            throw new SaveMessageWebsocketException(SaveMessageWebsocketException.Error.MESSAGE_DAO_SAVE_FAILED, e);
        }
    }
}
