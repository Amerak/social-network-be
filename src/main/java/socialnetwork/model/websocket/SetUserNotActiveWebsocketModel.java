package socialnetwork.model.websocket;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.exception.websocket.SetUserNotActiveWebsocketException;

import javax.inject.Inject;
import java.util.Objects;

@Component
public class SetUserNotActiveWebsocketModel {
    @Inject
    private UserDao userDao;

    public void setUserNotActive(WebSocketSession session){
        setNotActive(Objects.requireNonNull(session.getPrincipal()).getName());
    }

    private void setNotActive(String username){
        User user;
        try {
            user = userDao.findByUsername(username);
        } catch (Exception e) {
            throw new SetUserNotActiveWebsocketException(SetUserNotActiveWebsocketException.Error.USER_NOT_FOUND);
        }
        try {
            user.setActive(false);
            userDao.save(user);
        } catch (Exception e) {
            throw new SetUserNotActiveWebsocketException(SetUserNotActiveWebsocketException.Error.USER_DAO_SAVE_FAILED, e);
        }
    }
}
