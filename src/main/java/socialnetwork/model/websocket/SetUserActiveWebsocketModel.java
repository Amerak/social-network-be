package socialnetwork.model.websocket;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.exception.websocket.SetUserActiveWebsocketException;

import javax.inject.Inject;
import java.util.Objects;

@Component
public class SetUserActiveWebsocketModel {
    @Inject
    private UserDao userDao;

    public void setUserActive(WebSocketSession session) {
        setActive(Objects.requireNonNull(session.getPrincipal()).getName());
    }

    private void setActive(String username) {
        User user;
        try {
            user = userDao.findByUsername(username);
        } catch (Exception e) {
            throw new SetUserActiveWebsocketException(SetUserActiveWebsocketException.Error.USER_NOT_FOUND, e);
        }
        try {
            user.setActive(true);
            userDao.save(user);
        } catch (Exception e) {
            throw new SetUserActiveWebsocketException(SetUserActiveWebsocketException.Error.USER_DAO_SAVE_FAILED, e);
        }
    }
}
