package socialnetwork.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import socialnetwork.dto.user.*;
import socialnetwork.model.user.*;

import javax.inject.Inject;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
@RestController
public class UserController {

    @Inject
    private DeleteUserModel deleteUserModel;
    @Inject
    private EditUserModel editUserModel;
    @Inject
    private FindListUserModel findListUserModel;
    @Inject
    private GetUserModel getUserModel;
    @Inject
    private RegisterUserModel registerUserModel;
    @Inject
    private FollowUserModel followUserModel;
    @Inject
    private UnfollowUserModel unfollowUserModel;
    @Inject
    private GetCurrentUserModel getCurrentUserModel;

    @RequestMapping(value = "/user/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RegisterUserDtoOut register(@RequestBody RegisterUserDtoIn dtoIn) {
        return registerUserModel.register(dtoIn);
    }

    @RequestMapping(value = "/user/get", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetUserDtoOut getByIdentifires(@RequestBody GetUserDtoIn dtoIn) {
        return getUserModel.findUserByID(dtoIn);
    }

    @RequestMapping(value = "/user/getUserList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetUserListDtoOut getListOfUsers(@RequestBody GetUserListDtoIn dtoIn) {
        return findListUserModel.findList(dtoIn);
    }

    @RequestMapping(value = "/user/editUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public EditUserDtoOut editUser(@RequestBody EditUserDtoIn dtoIn) {
        return editUserModel.editUser(dtoIn);
    }

    @RequestMapping(value = "/user/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DeleteUserDtoOut deleteUser(@RequestBody DeleteUserDtoIn dtoIn) {
        return deleteUserModel.deleteUser(dtoIn);
    }

    @RequestMapping(value = "/user/follow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FollowUserDtoOut followUser(@RequestBody FollowUserDtoIn dtoIn) {
        return followUserModel.followUser(dtoIn);
    }

    @RequestMapping(value = "/user/unfollow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UnfollowUserDtoOut unFollowUser(@RequestBody UnfollowUserDtoIn dtoIn) {
        return unfollowUserModel.unfollowUser(dtoIn);
    }

    @RequestMapping(value = "/user/getCurrentUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetCurrentUserDtoOut getCurrentUser() {
        return getCurrentUserModel.getCurrentUser();
    }


}
