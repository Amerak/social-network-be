package socialnetwork.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import socialnetwork.dto.post.*;
import socialnetwork.model.post.CreatePostModel;
import socialnetwork.model.post.DeletePostModel;
import socialnetwork.model.post.FindListPostModel;

import javax.inject.Inject;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
@RestController
public class PostController {

    @Inject
    private CreatePostModel createPostModel;
    @Inject
    private FindListPostModel findListPostModel;
    @Inject
    private DeletePostModel deletePostModel;

    @RequestMapping(value = "/post/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CreatePostDtoOut createPost(@RequestBody CreatePostDtoIn dtoIn) {
        return createPostModel.createPost(dtoIn);
    }

    @RequestMapping(value = "/post/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetPostListDtoOut findList(@RequestBody GetPostListDtoIn dtoIn) {
        return findListPostModel.findList(dtoIn);
    }

    @RequestMapping(value = "/post/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DeletePostDtoOut findList(@RequestBody DeletePostDtoIn dtoIn) {
        return deletePostModel.deletePost(dtoIn);
    }
}
