package socialnetwork.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import socialnetwork.model.dataGeneration.GenerateDataModel;

import javax.inject.Inject;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
@RestController
public class DataGenerationController {

    @Inject
    private GenerateDataModel generateDataModel;

    @RequestMapping(value = "/dataGeneration/init", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteAndGenerateData() {
        generateDataModel.generateData();
        return "Successful";
    }
}
