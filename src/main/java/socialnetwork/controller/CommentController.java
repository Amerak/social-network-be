package socialnetwork.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import socialnetwork.dto.comment.*;
import socialnetwork.model.comment.CreateCommentModel;
import socialnetwork.model.comment.DeleteCommentModel;
import socialnetwork.model.comment.ListCommentModel;

import javax.inject.Inject;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
@RestController
public class CommentController {

    @Inject
    private CreateCommentModel createCommentModel;
    @Inject
    private ListCommentModel listCommentModel;
    @Inject
    private DeleteCommentModel deleteCommentModel;

    @RequestMapping(value = "/comment/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CreateCommentDtoOut createComment(@RequestBody CreateCommentDtoIn dtoIn) {
        return createCommentModel.createComment(dtoIn);
    }

    @RequestMapping(value = "/comment/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ListCommentDtoOut listComment(@RequestBody ListCommentDtoIn dtoIn) {
        return listCommentModel.listComment(dtoIn);
    }

    @RequestMapping(value = "/comment/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DeleteCommentDtoOut listComment(@RequestBody DeleteCommentDtoIn dtoIn) {
        return deleteCommentModel.deleteComment(dtoIn);
    }
}
