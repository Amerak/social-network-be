package socialnetwork.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import socialnetwork.dto.message.ListMessageDtoIn;
import socialnetwork.dto.message.ListMessageDtoOut;
import socialnetwork.model.message.FindListMessageModel;

import javax.inject.Inject;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
@RestController
public class MessageController {

    @Inject
    FindListMessageModel findListMessageModel;

    @RequestMapping(value = "/message/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ListMessageDtoOut findList(@RequestBody ListMessageDtoIn dtoIn) {
        return findListMessageModel.findList(dtoIn);
    }
}
