package socialnetwork.utils;

import org.springframework.web.socket.WebSocketSession;

import java.util.Objects;

public class SessionUtils {

    private static final String CHAT_NOTIFICATION_ENDPOINT = "/chat";

    public static String getLastPathSegment(WebSocketSession session) {
        String path = Objects.requireNonNull(session.getUri()).getPath();
        return Objects.requireNonNull(path.substring(path.lastIndexOf('/') + 1));
    }

    public static boolean isValidForSend(WebSocketSession receivedSession, WebSocketSession storedSession) {
        return receivedSession.equals(storedSession) || (getLastPathSegment(receivedSession).equals(Objects.requireNonNull(storedSession.getPrincipal()).getName())
                && getLastPathSegment(storedSession).equals(Objects.requireNonNull(receivedSession.getPrincipal()).getName()));
    }

    public static boolean isValidForNotify(WebSocketSession receivedSession, WebSocketSession storedSession){
        return Objects.requireNonNull(storedSession.getUri()).getPath().equals(CHAT_NOTIFICATION_ENDPOINT)
                && getLastPathSegment(receivedSession).equals(Objects.requireNonNull(storedSession.getPrincipal()).getName());
    }
}
