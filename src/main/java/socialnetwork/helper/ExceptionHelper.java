package socialnetwork.helper;

import socialnetwork.error.ErrorCode;
import socialnetwork.error.ErrorType;

public class ExceptionHelper {

    public static ErrorCode createErrorCode(String errorCode, ErrorType errorType) {
        if (ErrorType.SYSTEM.equals(errorType)) {
            return ErrorCode.system(errorCode);
        } else {
            return ErrorCode.application(errorCode);
        }
    }
}
