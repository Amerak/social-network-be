package socialnetwork.helper;

import org.springframework.http.HttpStatus;
import socialnetwork.error.ApplicationError;

public class HttpStatusHelper {

    public static HttpStatus resolveStatus(ApplicationError exception) {
        HttpStatus status;
        switch (exception.getCode().getType()) {
            case SYSTEM -> status = HttpStatus.INTERNAL_SERVER_ERROR;
            case APPLICATION -> status = HttpStatus.BAD_REQUEST;
            default -> status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return status;
    }
}
