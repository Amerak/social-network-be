package socialnetwork.helper;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import socialnetwork.domain.Message;

import javax.inject.Inject;

@Component
public class MessageHelper {

    @Inject
    private Gson gson;

    public Message decode(TextMessage textMessage){
        return gson.fromJson(textMessage.getPayload(), Message.class);
    }

    public TextMessage encode(Message message){
        return new TextMessage(gson.toJson(message));
    }
}