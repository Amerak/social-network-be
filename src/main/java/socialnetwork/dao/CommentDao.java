package socialnetwork.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import socialnetwork.domain.Comment;

import java.util.List;

public interface CommentDao extends MongoRepository<Comment, String> {
    List<Comment> findByPostIdInOrderByDateAsc(String id);
}
