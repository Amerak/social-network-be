package socialnetwork.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import socialnetwork.domain.User;

import java.util.List;

public interface UserDao extends MongoRepository<User, String> {
    List<User> findByIdIn(List<String> ids);
    User findByUsername(String username);
}
