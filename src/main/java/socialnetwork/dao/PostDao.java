package socialnetwork.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import socialnetwork.domain.Post;

import java.util.List;

public interface PostDao extends MongoRepository<Post, String> {
    List<Post> findByUserIdInOrderByDateDesc(List<String> ids);
    List<Post> findAllByOrderByDateDesc();
}
