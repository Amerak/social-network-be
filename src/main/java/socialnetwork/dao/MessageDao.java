package socialnetwork.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import socialnetwork.domain.Message;

import java.util.List;

public interface MessageDao extends MongoRepository<Message, String> {
    @Query(value = "{ " +
            "    $or:[" +
            "    {$and: [ { from: ?0 }, { to: ?1 } ] }," +
            "    {$and: [ { from:?1 }, { to:?0 } ] }" +
            "    ]" +
            "    })", sort = "{date : 1 }")
    List<Message> findPositionalParameters(String from, String to);
}
