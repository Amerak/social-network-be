package socialnetwork.dto.post;

public class DeletePostDtoOut {
    private String message;

    public DeletePostDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
