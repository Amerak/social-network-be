package socialnetwork.dto.post;

import javax.validation.constraints.NotBlank;

public class DeletePostDtoIn {

    @NotBlank
    private String postId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
