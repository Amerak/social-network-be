package socialnetwork.dto.post;

import java.util.List;

public class GetPostListDtoIn {

    private List<String> users;

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}
