package socialnetwork.dto.post;

import socialnetwork.domain.Post;

import java.util.List;

public class GetPostListDtoOut {
    private List<Post> itemList;

    public GetPostListDtoOut(List<Post> itemList) {
        this.itemList = itemList;
    }

    public List<Post> getItemList() {
        return itemList;
    }

    public void setItemList(List<Post> itemList) {
        this.itemList = itemList;
    }
}
