package socialnetwork.dto.post;

import javax.validation.constraints.NotBlank;

public class CreatePostDtoIn {

    @NotBlank
    private String content;
    @NotBlank
    private String header;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
