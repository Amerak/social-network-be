package socialnetwork.dto.comment;

public class DeleteCommentDtoOut {
    private String message;

    public DeleteCommentDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
