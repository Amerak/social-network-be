package socialnetwork.dto.comment;

import javax.validation.constraints.NotBlank;

public class CreateCommentDtoIn {

    @NotBlank
    private String content;
    @NotBlank
    private String postId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
