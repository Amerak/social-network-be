package socialnetwork.dto.comment;

public class CreateCommentDtoOut {
    private String message;

    public CreateCommentDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
