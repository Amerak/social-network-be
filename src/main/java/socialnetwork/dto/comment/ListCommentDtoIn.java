package socialnetwork.dto.comment;

import javax.validation.constraints.NotBlank;

public class ListCommentDtoIn {

    @NotBlank
    private String postId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
