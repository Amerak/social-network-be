package socialnetwork.dto.comment;

import socialnetwork.domain.Comment;

import java.util.List;

public class ListCommentDtoOut {
    private List<Comment> itemList;

    public ListCommentDtoOut(List<Comment> itemList) {
        this.itemList = itemList;
    }

    public List<Comment> getItemList() {
        return itemList;
    }

    public void setItemList(List<Comment> itemList) {
        this.itemList = itemList;
    }
}
