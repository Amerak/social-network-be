package socialnetwork.dto.comment;

import javax.validation.constraints.NotBlank;

public class DeleteCommentDtoIn {

    @NotBlank
    private String commentId;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }
}
