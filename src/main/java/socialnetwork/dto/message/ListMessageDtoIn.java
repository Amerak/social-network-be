package socialnetwork.dto.message;

import javax.validation.constraints.NotBlank;

public class ListMessageDtoIn {
    @NotBlank
    private String to;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
