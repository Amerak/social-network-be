package socialnetwork.dto.message;

import socialnetwork.domain.Message;

import java.util.List;

public class ListMessageDtoOut {
    List<Message> itemList;

    public ListMessageDtoOut(List<Message> itemList) {
        this.itemList = itemList;
    }

    public List<Message> getItemList() {
        return itemList;
    }

    public void setItemList(List<Message> itemList) {
        this.itemList = itemList;
    }
}
