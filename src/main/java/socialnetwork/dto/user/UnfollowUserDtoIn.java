package socialnetwork.dto.user;

import javax.validation.constraints.NotBlank;

public class UnfollowUserDtoIn {

    @NotBlank
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
