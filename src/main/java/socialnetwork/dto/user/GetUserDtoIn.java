package socialnetwork.dto.user;

import javax.validation.constraints.NotBlank;

public class GetUserDtoIn {

    @NotBlank
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
