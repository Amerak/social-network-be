package socialnetwork.dto.user;

import java.util.List;

public class GetCurrentUserDtoOut {
    private String id;
    private String firstName;
    private String surname;
    private String username;
    private String email;
    private List<String> followList;

    public GetCurrentUserDtoOut(String id, String firstName, String surname, String username, String email, List<String> followList) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.followList = followList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getFollowList() {
        return followList;
    }

    public void setFollowList(List<String> followList) {
        this.followList = followList;
    }
}
