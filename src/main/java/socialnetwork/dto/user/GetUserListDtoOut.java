package socialnetwork.dto.user;

import socialnetwork.domain.User;

import java.util.List;

public class GetUserListDtoOut {
    private List<User> itemList;

    public GetUserListDtoOut(List<User> itemList) {
        this.itemList = itemList;
    }

    public List<User> getItemList() {
        return itemList;
    }

    public void setItemList(List<User> itemList) {
        this.itemList = itemList;
    }
}
