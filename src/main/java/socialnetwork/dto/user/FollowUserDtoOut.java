package socialnetwork.dto.user;

public class FollowUserDtoOut {

    private String message;

    public FollowUserDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
