package socialnetwork.dto.user;

import javax.validation.constraints.NotBlank;

public class FollowUserDtoIn {

    @NotBlank
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
