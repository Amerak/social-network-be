package socialnetwork.dto.user;

public class DeleteUserDtoOut {
    private String message;

    public DeleteUserDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
