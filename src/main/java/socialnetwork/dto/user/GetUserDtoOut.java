package socialnetwork.dto.user;

public class GetUserDtoOut {
    private String id;
    private String firstName;
    private String surname;
    private String username;
    private String email;
    private Boolean isActive;

    public GetUserDtoOut(String id, String firstName, String surname, String username, String email, Boolean isActive) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.isActive = isActive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
