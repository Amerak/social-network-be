package socialnetwork.dto.user;

public class EditUserDtoOut {
    String message;

    public EditUserDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

