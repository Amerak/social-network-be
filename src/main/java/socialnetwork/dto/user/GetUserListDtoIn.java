package socialnetwork.dto.user;

import java.util.List;

public class GetUserListDtoIn {
    private List<String> users;

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}
