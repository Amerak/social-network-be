package socialnetwork.dto.user;

import org.springframework.data.annotation.Id;

public class DeleteUserDtoIn {
    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
