package socialnetwork.dto.user;

public class UnfollowUserDtoOut {
    private String message;

    public UnfollowUserDtoOut(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
