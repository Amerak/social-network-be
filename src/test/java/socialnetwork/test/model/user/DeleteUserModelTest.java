package socialnetwork.test.model.user;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.DeleteUserDtoIn;
import socialnetwork.model.user.DeleteUserModel;

import javax.inject.Inject;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class DeleteUserModelTest {

    @Inject
    private DeleteUserModel deleteUserModel;

    @Inject
    private UserDao userDao;

    @BeforeEach
    public void setUp() {
        userDao.deleteAll();
    }

    @After
    public void tearDown(){
        userDao.deleteAll();
    }

    @Test
    public void deleteUserWrongDaoTest(){
        DeleteUserDtoIn deleteUserDtoIn = new DeleteUserDtoIn();
        User user = new User();
        user.setUsername("Username");
        userDao.save(user);

        deleteUserDtoIn.setId(user.getId());

        Optional<User> optionalUser;
        optionalUser = userDao.findById(user.getId());
        assertTrue(optionalUser.isPresent());

        deleteUserModel.deleteUser(deleteUserDtoIn);
        optionalUser = userDao.findById(user.getId());
        assertTrue(optionalUser.isEmpty());
    }
}
