package socialnetwork.test.model.user;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.GetUserListDtoIn;
import socialnetwork.model.user.FindListUserModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class FindListUserModelTest {

    @Inject
    private FindListUserModel findListUserModel;

    @Inject
    private UserDao userDao;

    @BeforeEach
    public void setUp() {
        userDao.deleteAll();
    }

    @After
    public void tearDown() {
        userDao.deleteAll();
    }

    @Test
    public void findListUserByIdsTest() {
        GetUserListDtoIn getUserListDtoIn = new GetUserListDtoIn();
        User firstUser = new User();
        firstUser.setUsername("firstUser");
        userDao.save(firstUser);
        User secondUser = new User();
        secondUser.setUsername("firstUser");
        userDao.save(secondUser);
        List<String> list = new ArrayList<>();
        list.add(firstUser.getId());
        list.add(secondUser.getId());
        getUserListDtoIn.setUsers(list);

        List<User> userList = findListUserModel.findList(getUserListDtoIn).getItemList();
        assertEquals(2, userList.size());
    }

    @Test
    public void findListUserAllTest() {
        GetUserListDtoIn getUserListDtoIn = new GetUserListDtoIn();
        User firstUser = new User();
        firstUser.setUsername("firstUser");
        userDao.save(firstUser);
        User secondUser = new User();
        secondUser.setUsername("firstUser");
        userDao.save(secondUser);

        List<User> userList = findListUserModel.findList(getUserListDtoIn).getItemList();
        assertEquals(2, userList.size());
    }

}
