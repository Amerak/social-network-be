package socialnetwork.test.model.user;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.GetUserDtoIn;
import socialnetwork.dto.user.GetUserDtoOut;
import socialnetwork.exception.user.GetUserException;
import socialnetwork.model.user.GetUserModel;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class GetUserModelTest {

    @Inject
    private GetUserModel getUserModel;

    @Inject
    private UserDao userDao;

    @BeforeEach
    public void setUp() {
        userDao.deleteAll();
    }

    @After
    public void tearDown() {
        userDao.deleteAll();
    }

    @Test
    public void getUserHdsTest() {
        GetUserDtoIn getUserDtoIn = new GetUserDtoIn();
        User user = new User();
        user.setUsername("username");
        userDao.save(user);
        getUserDtoIn.setId(user.getId());

        GetUserDtoOut getUserDtoOut = getUserModel.findUserByID(getUserDtoIn);
        assertEquals(getUserDtoOut.getUsername(), user.getUsername());
    }

    @Test
    public void getUserUserNotFoundTest() {
        GetUserDtoIn getUserDtoIn = new GetUserDtoIn();
        getUserDtoIn.setId("wrongId");

        GetUserException thrown = assertThrows(
                GetUserException.class,
                () -> getUserModel.findUserByID(getUserDtoIn)
        );
        assertEquals(GetUserException.Error.USER_NOT_FOUND.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("User not found", thrown.getMessage());
    }

    @Test
    public void getUserUserInvalidDtoInTest() {
        GetUserDtoIn getUserDtoIn = new GetUserDtoIn();

        GetUserException thrown = assertThrows(
                GetUserException.class,
                () -> getUserModel.findUserByID(getUserDtoIn)
        );
        assertEquals(GetUserException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("DtoIn is not valid.", thrown.getMessage());
    }
}
