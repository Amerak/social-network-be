package socialnetwork.test.model.user;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.FollowUserDtoIn;
import socialnetwork.exception.user.FollowUserException;
import socialnetwork.model.user.FollowUserModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class FollowUserModelTest {

    @Inject
    private FollowUserModel followUserModel;

    @Inject
    private UserDao userDao;

    @BeforeEach
    public void setUp() {
        userDao.deleteAll();

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        User user = new User();
        userDao.save(user);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        SpringSecurityUser springSecurityUser = new SpringSecurityUser("username","password",true,true,true,true,authorities,user.getId());

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
    }

    @After
    public void tearDown(){
        userDao.deleteAll();
    }

    @Test
    public void followUserHdsTest() {
        FollowUserDtoIn followUserDtoIn = new FollowUserDtoIn();
        User user = new User();
        user.setUsername("username");
        userDao.save(user);
        followUserDtoIn.setUserId(user.getId());

        assertEquals("Successfully added", followUserModel.followUser(followUserDtoIn).getMessage());
    }

    @Test
    public void followUserUserNotFoundTest(){
        FollowUserDtoIn followUserDtoIn = new FollowUserDtoIn();
        followUserDtoIn.setUserId("wrongId");

        FollowUserException thrown = assertThrows(
                FollowUserException.class,
                () -> followUserModel.followUser(followUserDtoIn)
        );

        assertEquals(FollowUserException.Error.USER_NOT_FOUND.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("User not found", thrown.getMessage());
    }

    @Test
    public void followUserInvalidDtoInTest(){
        FollowUserDtoIn followUserDtoIn = new FollowUserDtoIn();
        FollowUserException thrown = assertThrows(
                FollowUserException.class,
                () -> followUserModel.followUser(followUserDtoIn)
        );
        assertEquals(FollowUserException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("DtoIn is not valid.", thrown.getMessage());

    }
}
