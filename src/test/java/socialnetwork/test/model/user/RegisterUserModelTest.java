package socialnetwork.test.model.user;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.user.RegisterUserDtoIn;
import socialnetwork.dto.user.RegisterUserDtoOut;
import socialnetwork.exception.user.RegisterUserException;
import socialnetwork.model.user.RegisterUserModel;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class RegisterUserModelTest {

    @Inject
    private RegisterUserModel userRegisterModel;

    @Inject
    private UserDao userDao;

    @BeforeEach
    public void setUp() {
        userDao.deleteAll();
    }

    @After
    public void tearDown(){
        userDao.deleteAll();
    }

    @Test
    public void registerUserHdsTest(){
        RegisterUserDtoIn registerUserDtoIn = new RegisterUserDtoIn();
        registerUserDtoIn.setUsername("Username");
        registerUserDtoIn.setFirstName("FirstName");
        registerUserDtoIn.setSurname("Surname");
        registerUserDtoIn.setEmail("test@email.cz");
        registerUserDtoIn.setPassword("password");
        registerUserDtoIn.setMatchingPassword("password");

        RegisterUserDtoOut dtoOut = userRegisterModel.register(registerUserDtoIn);

        Optional<User> user = userDao.findById(dtoOut.getId());
        assertTrue(user.isPresent());

        List<User> list = userDao.findAll();
        assertEquals(1, list.size());
    }


    @Test
    public void registerUserInvalidDtoInTest(){
        RegisterUserDtoIn registerUserDtoIn = new RegisterUserDtoIn();
        registerUserDtoIn.setFirstName("FirstName");
        registerUserDtoIn.setSurname("Surname");
        registerUserDtoIn.setEmail("test@email.cz");
        registerUserDtoIn.setPassword("password");
        registerUserDtoIn.setMatchingPassword("password");

        RegisterUserException thrown = assertThrows(
                RegisterUserException.class,
                () -> userRegisterModel.register(registerUserDtoIn)
        );

        assertEquals(RegisterUserException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("username: must not be blank", thrown.getCause().getMessage());
    }
}
