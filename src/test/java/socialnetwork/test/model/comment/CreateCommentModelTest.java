package socialnetwork.test.model.comment;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.CommentDao;
import socialnetwork.dao.PostDao;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.Post;
import socialnetwork.domain.User;
import socialnetwork.dto.comment.CreateCommentDtoIn;
import socialnetwork.exception.comment.CreateCommentException;
import socialnetwork.model.comment.CreateCommentModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class CreateCommentModelTest {

    @Inject
    private CreateCommentModel createCommentModel;

    @Inject
    private CommentDao commentDao;

    @Inject
    private UserDao userDao;

    @Inject
    private PostDao postDao;

    @BeforeEach
    public void setUp() {
        commentDao.deleteAll();

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        User user = new User();
        userDao.save(user);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        SpringSecurityUser springSecurityUser = new SpringSecurityUser("username","password",true,true,true,true,authorities,user.getId());

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
    }

    @After
    public void tearDown(){
        commentDao.deleteAll();
    }

    @Test
    public void createCommentHdsTest() {
        CreateCommentDtoIn createCommentDtoIn = new CreateCommentDtoIn();
        Post post = new Post();
        post.setContent("postContent");
        post.setHeader("postHeader");
        postDao.save(post);
        createCommentDtoIn.setContent("content");
        createCommentDtoIn.setPostId(post.getId());

        assertEquals("Comment created successfully", createCommentModel.createComment(createCommentDtoIn).getMessage());
    }

    @Test
    public void createCommentPostNotFoundTest(){
        CreateCommentDtoIn createCommentDtoIn = new CreateCommentDtoIn();
        createCommentDtoIn.setContent("content");
        createCommentDtoIn.setPostId("wrongId");

        CreateCommentException thrown = assertThrows(
                CreateCommentException.class,
                () -> createCommentModel.createComment(createCommentDtoIn)
        );

        assertEquals(CreateCommentException.Error.POST_NOT_FOUND.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("Post not found!", thrown.getMessage());
    }

    @Test
    public void createCommentInvalidDtoInTest(){
        CreateCommentDtoIn createCommentDtoIn = new CreateCommentDtoIn();

        CreateCommentException thrown = assertThrows(
                CreateCommentException.class,
                () -> createCommentModel.createComment(createCommentDtoIn)
        );

        assertEquals(CreateCommentException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("DtoIn is not valid.", thrown.getMessage());
    }
}
