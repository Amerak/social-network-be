package socialnetwork.test.model.comment;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.CommentDao;
import socialnetwork.dao.PostDao;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.Comment;
import socialnetwork.domain.Post;
import socialnetwork.domain.User;
import socialnetwork.dto.comment.ListCommentDtoIn;
import socialnetwork.exception.comment.ListCommentException;
import socialnetwork.model.comment.ListCommentModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class ListCommentModelTest {

    @Inject
    private ListCommentModel listCommentModel;

    @Inject
    private CommentDao commentDao;

    @Inject
    private UserDao userDao;

    @Inject
    private PostDao postDao;

    @BeforeEach
    public void setUp() {
        commentDao.deleteAll();

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        User user = new User();
        userDao.save(user);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        SpringSecurityUser springSecurityUser = new SpringSecurityUser("username", "password", true, true, true, true, authorities, user.getId());

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
    }

    @After
    public void tearDown() {
        commentDao.deleteAll();
    }

    @Test
    public void listCommentHdsTest() {
        ListCommentDtoIn listCommentDtoIn = new ListCommentDtoIn();
        Post post = new Post();
        post.setContent("Content");
        post.setHeader("Header");
        postDao.save(post);
        listCommentDtoIn.setPostId(post.getId());

        Comment comment = new Comment();
        comment.setPostId(post.getId());
        comment.setContent("Content");
        commentDao.save(comment);

        List<Comment> commentList = listCommentModel.listComment(listCommentDtoIn).getItemList();
        assertEquals(1, commentList.size());
    }

    @Test
    public void listCommentInvalidDtoInTest() {
        ListCommentDtoIn listCommentDtoIn = new ListCommentDtoIn();

        ListCommentException thrown = assertThrows(
                ListCommentException.class,
                () -> listCommentModel.listComment(listCommentDtoIn)
        );

        assertEquals(ListCommentException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("DtoIn is not valid.", thrown.getMessage());
    }

}
