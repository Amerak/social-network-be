package socialnetwork.test.model.message;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.MessageDao;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.dto.message.ListMessageDtoIn;
import socialnetwork.exception.message.FindListMessageException;
import socialnetwork.model.message.FindListMessageModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class FindListMessageModelTest {

    @Inject
    private FindListMessageModel findListMessageModel;
    @Inject
    private MessageDao messageDao;
    @Inject
    private UserDao userDao;

    @BeforeEach
    public void setUp() {
        messageDao.deleteAll();

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        User user = new User();
        userDao.save(user);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        SpringSecurityUser springSecurityUser = new SpringSecurityUser("username", "password", true, true, true, true, authorities, user.getId());

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
    }

    @After
    public void tearDown() {
        messageDao.deleteAll();
    }

    @Test
    public void findListMessageHdsTest() {
        ListMessageDtoIn listMessageDtoIn = new ListMessageDtoIn();
        listMessageDtoIn.setTo("userTo");
        User userTo = new User();
        userTo.setUsername("userTo");
        userDao.save(userTo);

        Message message = new Message();
        message.setFrom("username");
        message.setTo(userTo.getUsername());
        messageDao.save(message);

        List<Message> list = findListMessageModel.findList(listMessageDtoIn).getItemList();
        assertEquals(list.get(0).getId(),message.getId());
    }
    @Test
    public void findListMessageInvalidDtoIn(){
        ListMessageDtoIn listMessageDtoIn = new ListMessageDtoIn();

        FindListMessageException thrown = assertThrows(
                FindListMessageException.class,
                () -> findListMessageModel.findList(listMessageDtoIn)
        );

        assertEquals(FindListMessageException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("DtoIn is not valid", thrown.getMessage());
    }
}
