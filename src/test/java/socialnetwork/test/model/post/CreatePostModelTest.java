package socialnetwork.test.model.post;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import socialnetwork.SocialNetworkApplication;
import socialnetwork.configuration.ApplicationConfiguration;
import socialnetwork.configuration.service.SpringSecurityUser;
import socialnetwork.dao.PostDao;
import socialnetwork.dao.UserDao;
import socialnetwork.domain.User;
import socialnetwork.dto.post.CreatePostDtoIn;
import socialnetwork.exception.post.CreatePostException;
import socialnetwork.model.post.CreatePostModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@PropertySource("classpath:application.properties")
public class CreatePostModelTest {

    @Inject
    private CreatePostModel createPostModel;

    @Inject
    private UserDao userDao;

    @Inject
    private PostDao postDao;

    @BeforeEach
    public void setUp() {
        postDao.deleteAll();

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        User user = new User();
        userDao.save(user);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        SpringSecurityUser springSecurityUser = new SpringSecurityUser("username", "password", true, true, true, true, authorities, user.getId());

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
    }

    @After
    public void tearDown() {
        postDao.deleteAll();
    }

    @Test
    public void createPostHdsTest() {
        CreatePostDtoIn createPostDtoIn = new CreatePostDtoIn();
        createPostDtoIn.setContent("Content");
        createPostDtoIn.setHeader("Header");

        assertEquals("Post created successfully under this ID", createPostModel.createPost(createPostDtoIn).getMessage());
    }

    @Test
    public void createPostInvalidDtoInTest() {
        CreatePostDtoIn createPostDtoIn = new CreatePostDtoIn();

        CreatePostException thrown = assertThrows(
                CreatePostException.class,
                () -> createPostModel.createPost(createPostDtoIn)
        );

        assertEquals(CreatePostException.Error.INVALID_DTO_IN.getErrorCode().getErrorCode(), thrown.getCode().getErrorCode());
        assertEquals("DtoIn is not valid.", thrown.getMessage());
    }
}
