# Social Network BE



## Init
Command "dataGeneration/init" pro inicializaci aplikace s testovacími daty. 

Pro přihlášení do aplikace lze využít dva testovací usery:
- username: michal99, password: testPassword
- username: dominik98, password: testPassword

## Spuštění aplikace

Docker spustí mongodb i backend aplikaci.

```
cd docker
docker-compose up
```

## Insomnia
Soubor insomnia.json obsahuje nakonfigurovaný workspace pro insomnia API client.
Ke stažení: [https://insomnia.rest/](https://insomnia.rest/)

## Frontend
Repozitář: [https://gitlab.com/Amerak/social-network-fe](https://gitlab.com/Amerak/social-network-fe)